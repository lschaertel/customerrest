package com.customer.customerREST.Model;

public class CustomerEnums {
    public enum DNE {
        NC,
        Y,
        N
    }

    public enum CustVendorBlocked {
        No,
        Invoice,
        All
    }

    public enum CustAccountStatement {
        Always,
        Quarter,
        Binnually,
        Yearly,
        Never
    }

    public enum InvoiceOrderAccount {
        InvoiceAccount,
        OrderAccount
    }

    public enum DirPartyType {
        None,
        Person,
        Organization
    }
    public enum ATUCustSalutation {
        Mister,
        Mrs,
        Company
    }
    public enum ATUQuotationBy {
        Branch,
        Headquarter,
        None
    }
    public enum ATUFlagDneElecCdEnum {
        No,
        Yes,
        Disabled
    }
    public enum ATUFlagDnePostCdEnum {
        No,
        Yes,
        Disabled
    }
}