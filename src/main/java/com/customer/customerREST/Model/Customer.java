package com.customer.customerREST.Model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "CustTable")
@Getter
@Setter
public class Customer extends CustomerEnums{

    @Id
    @GeneratedValue(generator = "customer_generator")
    @SequenceGenerator(
            name = "customer_generator",
            sequenceName = "customer_generator",
            initialValue = 10000)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createdDateTime", nullable = false, updatable = false)
    @CreatedDate
    private Date createdDateTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modifiedDateTime", nullable = false, updatable = false)
    @LastModifiedDate
    private Date modifiedDateTime;

    private String name;
    private String address;
    private String phone;
    private String teleFax;
    private String invoiceAccount;
    private String custGroup;
    private String lineDisc;
    private String paymTermId;
    private String cashDisc;
    private String currency;
    private boolean interCompanyAutoCreateOrders;
    @Enumerated(EnumType.STRING)
    private CustVendorBlocked blocked;
    private boolean oneTimeCustomer;
    @Enumerated(EnumType.STRING)
    private CustAccountStatement accountStatement;
    private Double creditMax;
    private boolean mandatoryCreditLimit;
    private String dimension;
    private String vendAccount;
    private String telex;
    private String priceGroup;
    private String multiLineDisc;
    private String endDisc;
    private String vatNum;
    private String countryRegionId;
    private String inventLocation;
    private String dlvTerm;
    private String dlvMode;
    private String markupGroup;
    private String clearingPeriod;
    private String zipCode;
    private String state;
    private String county;
    private String url;
    private String email;
    private String cellularPhone;
    private String phoneLocal;
    private String freightZone;
    private String creditRating;
    private String taxGroup;
    private String statisticsGroup;
    private String paymMode;
    private String bankAccount;
    private String paymSched;
    private String nameAlias;
    private String contactPersonId;
    @Enumerated(EnumType.STRING)
    private InvoiceOrderAccount invoiceAddress;
    private String ourAccountNum;
    private String salesPoolId;
    private boolean inclTax;
    private String custItemGroupId;
    private String numberSequenceGroup;
    private String languageId;
    private String paymDayId;
    private String lineOfBusinessId;
    private String destinationCodeId;
    private String suppItemGroupId;
    private String taxLicenseNum;
    private String paymSpec;
    private String bankCentralBankPurposeText;
    private String bankCentralBankPurposeCode;
    private String city;
    private String street;
    private String pager;
    private String sms;
    private boolean interCompanyAllowIndirectCreation;
    private String dlvReason;
    private String salesCalendarId;
    private String custClassificationId;
    private boolean interCompanyDirectDelivery;
    private String inventSiteId;
    private String orderEntryDeadlineGroupId;
    @Enumerated(EnumType.STRING)
    private DirPartyType partyType;
    private String partyId;
    private String projpriceGroup;
    private String syncEntityId;
    private Integer syncVersion;
    private String identificationNumber;
    private String partyCountry;
    private String partyState;
    private String atuPaymTypeId;
    private String atuCustTypeID;
    private boolean atuCustCardIdCheck;
    private boolean atuCheckValidVehicles;
    private double atuExternalLimit;
    private double atuExternalLimitOpen;
    private Date atuBlockedDate;
    private String atuOrgName2_DEL;
    private String atuOrgName3_DEL;
    private String atuAuthorizationId;
    private String atuNoticeId;
    private String atuAuthNoticeTxtId;
    private String atuOrgName1_DEL;
    private Integer atuAddressHouseNumber;
    private String atuAddressHouseNumberAdd;
    private String atuAddressExtension;
    @Enumerated(EnumType.STRING)
    private ATUCustSalutation atuCustSalutation;
    private boolean atuCustDataUsagePermission;
    private double atuRevenueAct;
    private String atuInvoiceTypeId;
    private Date atuLastOrderDate;
    private String atuVipTxtId;
    private String atuValidityAreaId;
    private String atuEmplPersonnelNum_DEL;
    private boolean atuDiscEmplConfirm_DEL;
    private String atuInsuranceId;
    private ATUQuotationBy atuQuotationBy;
    private String atuAccountNumDBM;
    @Enumerated(EnumType.STRING)
    private ATUFlagDnePostCdEnum atuFlagDnePostCd;
    @Enumerated(EnumType.STRING)
    private ATUFlagDneElecCdEnum atuFlagDneElecCd;
    private boolean atuFlagDnePostChCd;
    private boolean atuFlagDneElecChCd;
    private String atuMainCustomer;
    private boolean atuInsuranceBlocked;
    private boolean atuFlagVehicleWhiteList;
    private boolean atuFlagBranchCust;
    private String atuCustInvoiceTag;
    private boolean atuCustOrderLikeQuote;
    private boolean atuDNEPost;
    private boolean atuDNEMail;
    private boolean atuDNESMS;
    private boolean atuDNEPhone;
    private boolean atuDNECancellation;
    @Enumerated(EnumType.STRING)
    private DNE atuDNEValueMail;
    @Enumerated(EnumType.STRING)
    private DNE atuDNEValuePhone;
    @Enumerated(EnumType.STRING)
    private DNE atuDNEValueSMS;
    @Enumerated(EnumType.STRING)
    private DNE atuDNEValuePost;
    private String atuMobiDataCustId;
    private Date atuArchivedDate;
    private Date atuInterfaceDate;
    private Integer atuValidAddressEmailDomain;
    private Integer atuValidAddressEmailSyntax;
    private boolean atuValidAddressEmail;
    private Integer atuLinkNumberPhone;
    private Integer atuLinkNumberCellphone;
    private Integer atuLinkNumberFax;
    private Integer atuLinkNumberEMail;
    private Integer atuErrorCodePhone;
    private Integer atuErrorCodeCellphone;
    private boolean atuArchived;
    private Integer atuQualityFAX;
    private Integer atuQualityCellphone;
    private Integer atuQualityEMail;
    private String atuQualityAddress;
    private Integer atuValidAddressStreet;
    private Integer atuLinkNumberAddress;

    public void setCustomer(Customer customer) {
        this.setName(customer.name);
        this.setAddress(customer.address);
        this.setPhone(customer.phone);
        this.setTeleFax(customer.teleFax);
        this.setInvoiceAccount(customer.invoiceAccount);
        this.setCustGroup(customer.custGroup);
        this.setLineDisc(customer.lineDisc);
        this.setPaymTermId(customer.paymTermId);
        this.setCashDisc(customer.cashDisc);
        this.setCurrency(customer.currency);
        this.setInterCompanyAutoCreateOrders(customer.interCompanyAutoCreateOrders);
        this.setBlocked(customer.blocked);
        this.setOneTimeCustomer(customer.oneTimeCustomer);
        this.setAccountStatement(customer.accountStatement);
        this.setCreditMax(customer.creditMax);
        this.setMandatoryCreditLimit(customer.mandatoryCreditLimit);
        this.setDimension(customer.dimension);
        this.setVendAccount(customer.vendAccount);
        this.setTelex(customer.telex);
        this.setPriceGroup(customer.priceGroup);
        this.setMultiLineDisc(customer.multiLineDisc);
        this.setEndDisc(customer.endDisc);
        this.setVatNum(customer.vatNum);
        this.setCountryRegionId(customer.countryRegionId);
        this.setInventLocation(customer.inventLocation);
        this.setDlvTerm(customer.dlvTerm);
        this.setDlvMode(customer.dlvMode);
        this.setMarkupGroup(customer.markupGroup);
        this.setClearingPeriod(customer.clearingPeriod);
        this.setZipCode(customer.zipCode);
        this.setState(customer.state);
        this.setCounty(customer.county);
        this.setUrl(customer.url);
        this.setEmail(customer.email);
        this.setCellularPhone(customer.cellularPhone);
        this.setPhoneLocal(customer.phoneLocal);
        this.setFreightZone(customer.freightZone);
        this.setCreditRating(customer.creditRating);
        this.setTaxGroup(customer.taxGroup);
        this.setStatisticsGroup(customer.statisticsGroup);
        this.setPaymMode(customer.paymMode);
        this.setBankAccount(customer.bankAccount);
        this.setPaymSched(customer.paymSched);
        this.setNameAlias(customer.nameAlias);
        this.setContactPersonId(customer.contactPersonId);
        this.setInvoiceAddress(customer.invoiceAddress);
        this.setOurAccountNum(customer.ourAccountNum);
        this.setSalesPoolId(customer.salesPoolId);
        this.setInclTax(customer.inclTax);
        this.setCustItemGroupId(customer.custItemGroupId);
        this.setNumberSequenceGroup(customer.numberSequenceGroup);
        this.setLanguageId(customer.languageId);
        this.setPaymDayId(customer.paymDayId);
        this.setLineOfBusinessId(customer.lineOfBusinessId);
        this.setDestinationCodeId(customer.destinationCodeId);
        this.setSuppItemGroupId(customer.suppItemGroupId);
        this.setTaxLicenseNum(customer.taxLicenseNum);
        this.setPaymSpec(customer.paymSpec);
        this.setBankCentralBankPurposeText(customer.bankCentralBankPurposeText);
        this.setBankCentralBankPurposeCode(customer.bankCentralBankPurposeCode);
        this.setCity(customer.city);
        this.setStreet(customer.street);
        this.setPager(customer.pager);
        this.setSms(customer.sms);
        this.setInterCompanyAllowIndirectCreation(customer.interCompanyAllowIndirectCreation);
        this.setDlvReason(customer.dlvReason);
        this.setSalesCalendarId(customer.salesCalendarId);
        this.setCustClassificationId(customer.custClassificationId);
        this.setInterCompanyDirectDelivery(customer.interCompanyDirectDelivery);
        this.setInventSiteId(customer.inventSiteId);
        this.setOrderEntryDeadlineGroupId(customer.orderEntryDeadlineGroupId);
        this.setPartyType(customer.partyType);
        this.setPartyId(customer.partyId);
        this.setProjpriceGroup(customer.projpriceGroup);
        this.setSyncEntityId(customer.syncEntityId);
        this.setSyncVersion(customer.syncVersion);
        this.setIdentificationNumber(customer.identificationNumber);
        this.setPartyCountry(customer.partyCountry);
        this.setPartyState(customer.partyState);
        this.setAtuPaymTypeId(customer.atuPaymTypeId);
        this.setAtuCustTypeID(customer.atuCustTypeID);
        this.setAtuCustCardIdCheck(customer.atuCustCardIdCheck);
        this.setAtuCheckValidVehicles(customer.atuCheckValidVehicles);
        this.setAtuExternalLimit(customer.atuExternalLimit);
        this.setAtuExternalLimitOpen(customer.atuExternalLimitOpen);
        this.setAtuBlockedDate(customer.atuBlockedDate);
        this.setAtuOrgName2_DEL(customer.atuOrgName2_DEL);
        this.setAtuOrgName3_DEL(customer.atuOrgName3_DEL);
        this.setAtuAuthorizationId(customer.atuAuthorizationId);
        this.setAtuNoticeId(customer.atuNoticeId);
        this.setAtuAuthNoticeTxtId(customer.atuAuthNoticeTxtId);
        this.setAtuOrgName1_DEL(customer.atuOrgName1_DEL);
        this.setAtuAddressHouseNumber(customer.atuAddressHouseNumber);
        this.setAtuAddressHouseNumberAdd(customer.atuAddressHouseNumberAdd);
        this.setAtuAddressExtension(customer.atuAddressExtension);
        this.setAtuCustSalutation(customer.atuCustSalutation);
        this.setAtuCustDataUsagePermission(customer.atuCustDataUsagePermission);
        this.setAtuRevenueAct(customer.atuRevenueAct);
        this.setAtuInvoiceTypeId(customer.atuInvoiceTypeId);
        this.setAtuLastOrderDate(customer.atuLastOrderDate);
        this.setAtuVipTxtId(customer.atuVipTxtId);
        this.setAtuValidityAreaId(customer.atuValidityAreaId);
        this.setAtuEmplPersonnelNum_DEL(customer.atuEmplPersonnelNum_DEL);
        this.setAtuDiscEmplConfirm_DEL(customer.atuDiscEmplConfirm_DEL);
        this.setAtuInsuranceId(customer.atuInsuranceId);
        this.setAtuQuotationBy(customer.atuQuotationBy);
        this.setAtuAccountNumDBM(customer.atuAccountNumDBM);
        this.setAtuFlagDnePostCd(customer.atuFlagDnePostCd);
        this.setAtuFlagDneElecCd(customer.atuFlagDneElecCd);
        this.setAtuFlagDnePostChCd(customer.atuFlagDnePostChCd);
        this.setAtuFlagDneElecChCd(customer.atuFlagDneElecChCd);
        this.setAtuMainCustomer(customer.atuMainCustomer);
        this.setAtuInsuranceBlocked(customer.atuInsuranceBlocked);
        this.setAtuFlagVehicleWhiteList(customer.atuFlagVehicleWhiteList);
        this.setAtuFlagBranchCust(customer.atuFlagBranchCust);
        this.setAtuCustInvoiceTag(customer.atuCustInvoiceTag);
        this.setAtuCustOrderLikeQuote(customer.atuCustOrderLikeQuote);
        this.setAtuDNEPost(customer.atuDNEPost);
        this.setAtuDNEMail(customer.atuDNEMail);
        this.setAtuDNESMS(customer.atuDNESMS);
        this.setAtuDNEPhone(customer.atuDNEPhone);
        this.setAtuDNECancellation(customer.atuDNECancellation);
        this.setAtuDNEValueMail(customer.atuDNEValueMail);
        this.setAtuDNEValuePhone(customer.atuDNEValuePhone);
        this.setAtuDNEValueSMS(customer.atuDNEValueSMS);
        this.setAtuDNEValuePost(customer.atuDNEValuePost);
        this.setAtuMobiDataCustId(customer.atuMobiDataCustId);
        this.setAtuArchivedDate(customer.atuArchivedDate);
        this.setAtuInterfaceDate(customer.atuInterfaceDate);
        this.setAtuValidAddressEmailDomain(customer.atuValidAddressEmailDomain);
        this.setAtuValidAddressEmailSyntax(customer.atuValidAddressEmailSyntax);
        this.setAtuValidAddressEmail(customer.atuValidAddressEmail);
        this.setAtuLinkNumberPhone(customer.atuLinkNumberPhone);
        this.setAtuLinkNumberCellphone(customer.atuLinkNumberCellphone);
        this.setAtuLinkNumberFax(customer.atuLinkNumberFax);
        this.setAtuLinkNumberEMail(customer.atuLinkNumberEMail);
        this.setAtuErrorCodePhone(customer.atuErrorCodePhone);
        this.setAtuErrorCodeCellphone(customer.atuErrorCodeCellphone);
        this.setAtuArchived(customer.atuArchived);
        this.setAtuQualityFAX(customer.atuQualityFAX);
        this.setAtuQualityCellphone(customer.atuQualityCellphone);
        this.setAtuQualityEMail(customer.atuQualityEMail);
        this.setAtuQualityAddress(customer.atuQualityAddress);
        this.setAtuValidAddressStreet(customer.atuValidAddressStreet);
        this.setAtuLinkNumberAddress(customer.atuLinkNumberAddress);
    }
}
