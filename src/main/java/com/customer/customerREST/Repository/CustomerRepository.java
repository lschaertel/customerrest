package com.customer.customerREST.Repository;

import com.customer.customerREST.Model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    @Query("SELECT c from Customer c where c.id = ?1")
    List<Customer> findByCustomerId(Long customerId);
}