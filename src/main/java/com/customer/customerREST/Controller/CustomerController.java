package com.customer.customerREST.Controller;

import com.customer.customerREST.Exception.ResourceNotFoundException;
import com.customer.customerREST.Model.Customer;
import com.customer.customerREST.Repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class CustomerController {
    @Autowired
    private CustomerRepository customerRepository;

    @GetMapping("/customers")
    public Page<Customer> getCustomer(Pageable pageable) {
        return customerRepository.findAll(pageable);
    }

    @GetMapping("/customers/{customerId]")
    public List<Customer> getCustomerById(@PathVariable Long customerId) {
        return customerRepository.findByCustomerId(customerId);
    }

    @PostMapping("/customers")
    public Customer addCustomer(@Valid @RequestBody Customer customer) {
        return customerRepository.save(customer);
    }

    @PutMapping("/customers/{customerId}")
    public Customer updateCustomer(@PathVariable Long customerId,
                                   @Valid @RequestBody Customer customerRequest) {
        return customerRepository.findById(customerId)
                .map(customer -> {
                    customer.setCustomer(customer);
                    return customerRepository.save(customer);
                }).orElseThrow(() -> new ResourceNotFoundException("Customer not found with id " + customerId));
    }
    @DeleteMapping("/customers/{customerId}")
    public ResponseEntity<?> deleteCustomer(@PathVariable Long customerId) {
        return customerRepository.findById(customerId)
                .map(customer -> {
                    customerRepository.delete(customer);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Customer not found with id " + customerId));
    }
}
